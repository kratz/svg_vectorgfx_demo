# Proof-of-concept: play vector-based presentations with viewports embedded in the SVG

This code is a JavaScript-based player which zooms through a series of viewports. The viewports are defined as SVG rectangles. This makes it possible, in principle, to use any vector-drawing software (Inkscape, Illustrator, AutoCAD...) to create vector-based presentations.

In the example, the viewports are black rectangles, but the color of the rectangles can be set to fully transparent.
